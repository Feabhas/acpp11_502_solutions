#ifndef TRACE_DEBUG_H_
#define TRACE_DEBUG_H_

#ifdef TRACE_ENABLED
    #include <iostream>
    #define TRACE(msg) std::cout << "DEBUG : " << msg << std::endl
#else
    #define TRACE(msg)
#endif

#endif