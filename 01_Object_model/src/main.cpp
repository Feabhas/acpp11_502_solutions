// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include <iostream>
#include <iomanip>

using namespace std;

int init_global { 10 };
int uninit_global;
int zero_global { 0 };
const int const_global { 100 };
static int static_global { 200 };


int main()
{
    int local { 1 };
    static int static_auto { 2 };
    
    cout << hex << showbase;
    cout << "initialised global      : " << &init_global   << endl;
    cout << "Uninitialised global    : " << &uninit_global << endl;
    cout << "Zero-initialised global : " << &zero_global   << endl;
    cout << "Constant global         : " << &const_global  << endl;
    cout << "Static global           : " << &static_global << endl;
    cout << "Automatic               : " << &local         << endl;
    cout << "Static automatic        : " << &static_auto   << endl;
}
