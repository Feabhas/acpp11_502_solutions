// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#ifndef TEMPLATEBUFFER_H
#define TEMPLATEBUFFER_H

#include <cstddef>
#include <stdexcept>
#include <array>


// Buffer exception classes, derived from
// std::out_of_range.
//
class buffer_empty : public std::out_of_range {
public:
    buffer_empty() : out_of_range{ "Buffer empty!" }    {}
    buffer_empty(const char* str) : out_of_range{ str } {}
};


class buffer_full : public std::out_of_range {
public:
    buffer_full() : out_of_range{ "Buffer full!" }     {}
    buffer_full(const char* str) : out_of_range{ str } {}
};



template <typename T = int, std::size_t sz = 8>
class Buffer {
public:
    void add(const T& in_val);
    void add(T&& in_val);
    T get();
    bool is_empty() const;
    std::size_t size() const;
  
private:
    using Container = std::array<T, sz>;
    using Iterator  = typename Container::iterator;

    Container buffer   { }; 
    Iterator read      { std::begin(buffer) };
    Iterator write     { std::begin(buffer) };
    unsigned num_items { 0 };
};


template <typename T, std::size_t sz>
void Buffer<T, sz>::add(const T& in_val)
{
    if (num_items == sz) throw buffer_full { };

    *write = in_val;  
    ++num_items;
    ++write;
    if (write == std::end(buffer)) write = std::begin(buffer);
}


template <typename T, std::size_t sz>
void Buffer<T, sz>::add(T&& in_val)
{
    if (num_items == sz) throw buffer_full { };

    *write = std::move(in_val);
    ++num_items;
    ++write;
    if (write == std::end(buffer)) write = std::begin(buffer);
}


template <typename T, std::size_t sz>
T Buffer<T, sz>::get()
{
    if (num_items == 0) throw buffer_empty { };

    auto ret_val_iter { read };
    --num_items;
    ++read;
    if (read == std::end(buffer)) read = std::begin(buffer);

    return std::move(*ret_val_iter);
}


template <typename T, std::size_t sz>
bool Buffer<T, sz>::is_empty() const
{
    return (num_items == 0);
}


template <typename T, std::size_t sz>
std::size_t Buffer<T, sz>::size() const
{
    return num_items;
}


#endif // TEMPLATEBUFFER_H
