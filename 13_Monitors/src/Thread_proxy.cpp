#include <pthread.h>
#include <iostream>
#include "Thread_proxy.h"

using namespace std;

void Thread::join()
{
    handle = native_handle();
    thread::join();
}


void Thread::detach()
{
    handle = native_handle();
    thread::detach();
}


Thread::~Thread()
{
    cout << "Terminating thread " << name << endl;

    if (joinable()) {
        cout << "WARNING: Killing joinable thread"  << endl;
        detach();
    }

    pthread_cancel(handle);
}