
#include <chrono>
#include <thread>
#include <atomic>
#include "Generator.h"
#include "Display.h"
#include "Alarm_filter.h"
#include "Pipe.h"
#include "Pipeline.h"
#include "Thread_proxy.h"


using namespace std;
using namespace chrono_literals;


// Atomic flag to allow the Generator
// thread to signal the other threads
//
atomic<bool> done { false };


int main()
{
    Generator    generator { };
    Display      display   { };
    Alarm_filter filter    { Alarm::advisory };
    
    Pipe pipe1 { };
    Pipe pipe2 { };

    connect(generator, pipe1);
    connect(filter, pipe1, pipe2);
    connect(display, pipe2);

    auto run_n_times =  [](int num_times, Filter& filter) 
                        {
                            for (int i { 0 }; i < num_times; ++i) {
                                filter.execute();
                                this_thread::sleep_for(1000ms);
                            }
                            done = true;
                        };

    auto run_forever =  [](Filter& filter)
                        {
                            while (!done) {
                                filter.execute();
                                this_thread::yield();
                            }
                        };

    Thread gen_thread     { "Generator", run_n_times, 5, ref(generator) };
    Thread filter_thread  { "ID Filter", run_forever, ref(filter) };
    Thread display_thread { "Display",   run_forever, ref(display) };
    
    gen_thread.join();
    filter_thread.join();
    display_thread.join();
}