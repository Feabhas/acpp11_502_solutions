// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#ifndef MONITOR_BUFFER_H_
#define MONITOR_BUFFER_H_

#include <mutex>
#include <condition_variable>
#include "Buffer.h"


template <typename T, std::size_t sz>
class Monitor_buffer : private Buffer<T, sz> {
public:
    template <typename U> void add(U&& in);
    T get();
    bool is_empty() const;
    std::size_t size() const;

private:
    using Base = Buffer<T, sz>;

    // mutex must be mutable for
    // use with const member
    // functions
    //
    mutable std::mutex mtx { };
    std::condition_variable has_data  { };
    std::condition_variable has_space { };
};


template <typename T, std::size_t sz>
template <typename U> 
void Monitor_buffer<T, sz>::add(U&& in)
{
    std::unique_lock<std::mutex> lock { mtx };

    while (Base::size() == sz) {
        has_space.wait(lock);
    }

    Base::add(std::forward<U>(in));
    has_data.notify_one();
}


template <typename T, std::size_t sz>
T Monitor_buffer<T, sz>::get()
{
    std::unique_lock<std::mutex> lock { mtx };

    while (Base::is_empty()) {
        has_data.wait(lock);
    }
    
    auto out = Base::get();
    has_space.notify_one();
    return out;
}


template <typename T, std::size_t sz>
bool Monitor_buffer<T, sz>::is_empty() const
{
    std::lock_guard<std::mutex> lock { mtx };
    return Base::is_empty();
}


template <typename T, std::size_t sz>    
std::size_t Monitor_buffer<T, sz>::size() const
{
    std::lock_guard<std::mutex> lock { mtx };
    return Base::size();
}


#endif