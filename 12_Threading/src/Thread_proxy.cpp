#include <pthread.h>
#include <iostream>
#include "Thread_proxy.h"

using namespace std;


Thread::~Thread()
{
    cout << "Terminating thread " << name << endl;

    if (joinable()) {
        cout << "WARNING: Killing joinable thread"  << endl;
        detach();
    }
}