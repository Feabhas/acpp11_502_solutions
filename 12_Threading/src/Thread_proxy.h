#ifndef THREAD_PROXY
#define THREAD_PROXY

#include <thread>
#include <string>

// Thread acts as a 'safe' std::thread, in
// that any running (detached) threads that exist
// are terminated in the OS
//
class Thread : private std::thread {
public:
    template<typename Callable_Ty, typename... Param_Ty> 
    Thread(std::string text, Callable_Ty&& thread_func, Param_Ty&&... param) :
        std::thread { std::forward<Callable_Ty>(thread_func), std::forward<Param_Ty>(param)... },
        name        { std::move(text) }
    {
    }
    
    ~Thread();

    using std::thread::join;
    using std::thread::detach;

private:
    std::string name;
};


#endif